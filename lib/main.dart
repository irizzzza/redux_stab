import 'package:bogdan_redux_first_app/app_state.dart';
import 'package:bogdan_redux_first_app/pages/about.dart';
import 'package:bogdan_redux_first_app/pages/home.dart';
import 'package:bogdan_redux_first_app/pages/settings.dart';
import 'package:bogdan_redux_first_app/reducers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';



void main() {
  final _initialState = AppState(sliderFontSize: 0.5);
// final Store<AppState> _store =
// Store<AppState>(reducer, initialState: _initialState);
  final _store = new Store<AppState>(combineReducers<AppState>([reducer]),
      initialState: _initialState,
      middleware: [
       // NavigationMiddleware<AppState>(),
      ]);
  runApp(MyApp(store: _store));
}
class MyApp extends StatelessWidget {
  final Store<AppState> store;
  MyApp({this.store});
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        initialRoute: '/',
        routes: {
          '/': (context) => Home(),
          '/about': (context) => About(),
          '/settings': (context) => Settings(),
        },
      ),
    );
  }
}
